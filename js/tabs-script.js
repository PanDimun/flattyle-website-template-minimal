let sliderNav = document.querySelectorAll('.slider-nav__items');
let sliderContent = document.querySelectorAll('.slider__text');

let showcaseNav = document.querySelectorAll('.showcase-nav__items');
let showcaseContent = document.querySelectorAll('.showcase');

function tabs(tabNav, tabContent) {
    
    tabNav.forEach(item => {
        item.addEventListener('click', selectTabNav)
    });
    function selectTabNav() {
        tabNav.forEach(item => {
            item.classList.remove('is-active');
        });
        this.classList.add('is-active');
        tabName = this.getAttribute('data-tab-name');
        selectTabContent(tabName);
    }

    function selectTabContent(tabName) {
        tabContent.forEach(item => {
            item.classList.contains(tabName) ? item.classList.add('is-active') : item.classList.remove('is-active');
        })
    }

};

tabs(sliderNav, sliderContent);
tabs(showcaseNav, showcaseContent);